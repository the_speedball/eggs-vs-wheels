# Research

A bunch of links:

- https://packaging.python.org/tutorials/distributing-packages/#wheels
- https://packaging.python.org/discussions/wheel-vs-egg/
- http://lucumr.pocoo.org/2014/1/27/python-on-wheels/
- https://www.python.org/dev/peps/pep-0427/
- https://pythonwheels.com/
- http://mrtopf.de/en/a-small-introduction-to-python-eggs/
- http://peak.telecommunity.com/DevCenter/PythonEggs
